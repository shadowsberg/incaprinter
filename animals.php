<?php
require __DIR__ . '/vendor/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;

$id = $_GET['id'];
$fecha = date("d/m/Y H:i:s", strtotime($_GET['fecha']));
$caja = $_GET['caja'];
$total = $_GET['total'];
$ticket = $_GET['ticket'];
$ticket = json_decode($ticket, true);

$connector = null;
$connector = new WindowsPrintConnector("Print");
$printer = new Printer($connector);
$printer->text("Ticket: ".$id."\n");
$printer->text("Fecha: ".$fecha."\n");
$printer->text("Caja: ".$caja."\n");
$printer->text("--------------------------------\n");
$printer->text("APUESTA     SORTEO        MONTO\n");
$printer->text("--------------------------------\n");
foreach ($ticket as $item) {
	$printer->text($item['number']." ".$item['name']." ".$item['time']."    ".$item['ammount']."\n");
}
$printer->text("--------------------------------\n");
$printer->text("TOTAL: ".number_format($total,0,',','.')."\n");
$printer->feed(3);
$printer->cut();
$printer->close();
header("Location: http://158.69.156.218/seller/lotery");
die();