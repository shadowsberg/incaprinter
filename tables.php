<?php
require __DIR__ . '/vendor/autoload.php';
include "Barcode.php"; 
use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
date_default_timezone_set('America/Caracas');
$codebar = $_GET['codebar'];;
$id = $_GET['id'];
$fecha = date("d/m/Y H:i:s", strtotime($_GET['fecha']));
$caja = $_GET['caja'];
$total = $_GET['total'];
$hipodromo = $_GET['hipodromo'];
$ticket = $_GET['ticket'];
$program = $_GET['program'];
$race = $_GET['race'];
$ticket = json_decode($ticket, true);

$connector = null;
$connector = new WindowsPrintConnector("Print");
$printer = new Printer($connector);
$bc = new Barcode39($id); 
$bc->barcode_text_size = 5;  
$bc->barcode_bar_thick = 8;  
$bc->barcode_bar_thin = 4; 
$bc->draw("codes/barcode.gif");

$printer->setJustification(Printer::JUSTIFY_CENTER);
$printer->text("Ticket: ".$id."\n");
$printer->text("Fecha: ".$fecha."\n");
$printer->text("Caja: ".$caja."\n");
$printer->text("Hipódromo:  ".$hipodromo."\n");
$printer->text("Carrera:  ".$race."\n");
$printer->text("--------------------------------\n");
$printer->setJustification(Printer::JUSTIFY_LEFT);
$printer->text("EJEM          CANT        PRECIO\n");
$printer->text("--------------------------------\n");
foreach ($ticket as $item) {
	$printer->text($item['pivot']['number']."-".$item['name']."  ".$item['pivot']['units']."  ".$item['pivot']['price']."\n");
}
$printer->text("--------------------------------\n");
$printer->setJustification(Printer::JUSTIFY_CENTER);
$printer->setEmphasis(true);
$printer->text("TOTAL: ".number_format($total,0,',','.')."\n");
$printer->setEmphasis(false);
if($codebar == 1){
	$printer->setJustification(Printer::JUSTIFY_CENTER);
	$tux = EscposImage::load("codes/barcode.gif", false);
	$printer->bitImage($tux);
}else{
	$printer->text("CONTROL: " .substr(md5($id), 0,4)."\n");
}
$printer->setEmphasis(false);
$printer->text("Revise su ticket\n");
$printer->text("Caduca a los 5 dias\n");
$printer->feed(3);
$printer->cut();
$printer->close();
header("Location: http://158.69.156.218/seller/programs/".$program);
die();