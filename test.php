<?php
require __DIR__ . '/vendor/autoload.php';
include "Barcode.php"; 
use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
date_default_timezone_set('America/Caracas');


$connector = null;
$connector = new WindowsPrintConnector("Print");
$printer = new Printer($connector);
$bc = new Barcode39(4215); 
$bc->barcode_text_size = 5;  
$bc->barcode_bar_thick = 8;  
$bc->barcode_bar_thin = 4; 
$bc->draw("codes/barcode.gif");

$printer->setJustification(Printer::JUSTIFY_CENTER);
$printer->text("Ticket: test \n");
$printer->text("Fecha: xx/xx/xxxx\n");
$printer->text("Caja: UNO\n");
$printer->text("Hipódromo:  Test\n");
$printer->text("Carrera:  Test\n");
$printer->text("--------------------------------\n");
$printer->setJustification(Printer::JUSTIFY_LEFT);
$printer->text("EJEM      TIPO   CANT\n");
$printer->text("--------------------------------\n");
$printer->text("1 - Ejemplar Uno WIN  2.500 \n");
$printer->text("1 - Ejemplar Dos WIN  2.800 \n");
$printer->text("--------------------------------\n");
$printer->text("TOTAL: XXX \n");

$printer->setEmphasis(false);
$printer->setJustification(Printer::JUSTIFY_CENTER);
$tux = EscposImage::load("codes/barcode.gif", false);
$printer->bitImage($tux);
$printer->text("CONTROL: AF23\n");

$printer->setEmphasis(false);
$printer->text("Revise su ticket\n");
$printer->text("Caduca a los 5 dias\n");
$printer->feed(3);
$printer->cut();
$printer->close();
header("Location: http://incasports.net/seller");
die();